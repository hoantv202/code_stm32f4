#include "stm32f4xx.h"
#include <stdint.h>
#include "string.h"


//#define RXSIXE 20
//uint8_t RxBuf[20];
//uint8_t MaiBuf[50];
//uint8_t index =0 ;

//****USART_DMA*****
volatile char str1[15]={"Hello world"};
volatile char str2[15];

#ifndef addUSART_DR
   #define addUSART_DR      ((uint32_t)0x400044014)  // 0x400044000+0x14+0x18xStream number
#endif

// **************ADC*************
uint16_t ADC_VAL[2]= {0,0};

//**************ADC_DMA***********
//uint16_t RxData[2];
uint16_t RxData[3];
float Temperature;

//******************************

typedef struct{
	uint8_t Hours;
	uint8_t Minutes;
	uint8_t Seconds;
}RTC_TimeTypeDef;

typedef struct{
	uint8_t Month;
	uint8_t Day;
	uint8_t Year;
}RTC_DateTypeDef;

RTC_TimeTypeDef sTime;
RTC_DateTypeDef sDate;


void delay_us(uint32_t us)
{
	while(us--);
}

void SysClockConfig (void)
{
		/*************>>>>>>> STEPS FOLLOWED <<<<<<<<************

	1. ENABLE HSE and wait for the HSE to become Ready
	2. Set the POWER ENABLE CLOCK and VOLTAGE REGULATOR
	3. Configure the FLASH PREFETCH and the LATENCY Related Settings
	4. Configure the PRESCALARS HCLK, PCLK1, PCLK2
	5. Configure the MAIN PLL
	6. Enable the PLL and wait for it to become ready
	7. Select the Clock Source and wait for it to be set

	********************************************************/


	#define PLL_M 	4
	#define PLL_N 	180
	#define PLL_P 	0  // PLLP = 2

	// 1. ENABLE HSE and wait for the HSE to become Ready
	RCC->CR |= RCC_CR_HSEON;
	while (!(RCC->CR & RCC_CR_HSERDY));

	// 2. Set the POWER ENABLE CLOCK and VOLTAGE REGULATOR
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	PWR->CR |= PWR_CR_VOS;


	// 3. Configure the FLASH PREFETCH and the LATENCY Related Settings
	FLASH->ACR = FLASH_ACR_ICEN | FLASH_ACR_DCEN | FLASH_ACR_PRFTEN | FLASH_ACR_LATENCY_5WS;

	// 4. Configure the PRESCALARS HCLK, PCLK1, PCLK2
	// AHB PR
	RCC->CFGR |= RCC_CFGR_HPRE_DIV1;

	// APB1 PR
	RCC->CFGR |= RCC_CFGR_PPRE1_DIV4;

	// APB2 PR
	RCC->CFGR |= RCC_CFGR_PPRE2_DIV2;


	// 5. Configure the MAIN PLL
	RCC->PLLCFGR = (PLL_M <<0) | (PLL_N << 6) | (PLL_P <<16) | (RCC_PLLCFGR_PLLSRC_HSE);

	// 6. Enable the PLL and wait for it to become ready
	RCC->CR |= RCC_CR_PLLON;
	while (!(RCC->CR & RCC_CR_PLLRDY));

	// 7. Select the Clock Source and wait for it to be set
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL);
}


void IO_Config()
{
	RCC->AHB1ENR |= (1<<3); // Port D
	RCC->AHB1ENR |= (1<<0); // Port A
	GPIOD->MODER |= (1<<24);

}
//*******************************RTC**************************************************
void RTC_Init(void)
{
	// Enable peripheral clock power
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	// Enable access to the RTC registers
	PWR->CR |= PWR_CR_DBP;
	// Enable LSI clock for RTC and wait for ready flag
	RCC->CSR |= RCC_CSR_LSION;
	while(!(RCC->CSR & RCC_CSR_LSIRDY));
	// Select LSI as clock source and enable RTC
	RCC->BDCR |= 0x8200;
	// Enter the "key" to unlock write protection
	RTC->WPR |= 0xCA;
	RTC->WPR |= 0x53;
	// Set INIT bit and wait for ready flag
	RTC->ISR |= RTC_ISR_INIT;
	while(!(RTC->ISR & RTC_ISR_INITF));
	// Adjust prescaler values for RTC to obtain 1 Hz --> (127+1)x(249+1)
	RTC->PRER |= 0xF9;
	RTC->PRER |= 0x7F<<16;
	// Write time and date values
	RTC->TR |= 0x130000;
	RTC->TR |= 0x5700;
	RTC->DR |= 0x215124;
	// Set BYPSHAD bit
	RTC->CR |= RTC_CR_BYPSHAD;
	// Clear INIT bit
	RTC->ISR &= ~RTC_ISR_INIT;
	// Disable access to RTC register
	PWR->CR &= ~PWR_CR_DBP;
}

void RTC_get_time(void)
{
	sTime.Seconds = (((RTC->TR & 0x7f)>>4)*10)+(RTC->TR & 0xf);
	sTime.Minutes = ((RTC->TR & 0x7f00)>>8);
	sTime.Minutes = (((sTime.Minutes & 0x7f)>>4)*10+(sTime.Minutes & 0x7f));
	sTime.Hours = ((RTC->TR & 0x7f0000)>>16);
    sTime.Hours = (((sTime.Hours & 0x7f)>>4)*10)+(sTime.Hours & 0xf);
}

void RTC_get_date(void)
{
	sDate.Year = ((RTC->DR >> 20)*10) + ((RTC->DR >> 16) & 0xf);
	sDate.Month = ((RTC->DR >> 12) & 1)*10 + ((RTC->DR >> 8) & 0xf);
	sDate.Day = ((RTC->DR >>4) & 3)*10 + (RTC->DR & 0xf);
}
// ************************ USART2 ***************************************
void USART2_Config(void)
{
	/*********   Steps followed ***************

	1. Enable the UART CLOCK and GPIO Clock
	2. Configure the UART PINs for Alternate Functions
	3. Enable the UART by writing the UE bit in USART_CR1 register to 1.
	4. Program the M bit in USART_CR1 to define the word length.
	5. Select the desired baud rate using the UART_BRR register.
	6. Enable the Transmitter/Receiver by Setting the TE and RE bits in USART_CR1 register.

	*******************************************/

	// 1.Enable the UART CLOCK and GPIO Clock
	RCC->APB1ENR |= (1<<17);
	RCC->AHB1ENR |= (1<<0);
	// 2.Configure the UART PINs for Alternate Functions
	GPIOA->MODER |= (2<<4); // Bits(5:4)=1:0--> Alternate Functions for Pin PA2
	GPIOA->MODER |= (2<<6); // Bits(7:6)=1:0--> Alternate Functions for Pin PA3
	GPIOA->OSPEEDR |= (3<<4)|(3<<6); // Bits (5:4) = 1:1 and Bits (7:6)=1:1 -> High Speed for PIN PA2 and PA3
	GPIOA->AFR[0] |= (7<<8); // Bytes (11:10:9:8) = 0:1:1:1 -> AF7 Alternate Functions for USART2 at PIN PA2
	GPIOA->AFR[0] |=(7<<12); // Bytes (15:14:13:12) = 0:1:1:1 -> AF7 Alternate Functions for USART2 at PIN PA3
	// 3.Enable the UART by writing the UE bit in USART_CR1 register to 1.
	USART2->CR1 = 0x00; // Clear all
	USART2->CR1 |= (1<<13); // USART enable "bit UE =1 "
	// 4.Program the M bit in USART_CR1 to define the word length.
	USART2 ->CR1 &= ~(1<<12); // M=0 , 1 bit start ,8 data, n bit stop
	USART2->CR2 =0x00; // 1 bit stop
	// 5.Select the desired baud rate using the UART_BRR register.
//	USART2->BRR = (13<<0)|(22<<4); // Baud rate of 115200, PLCK1 at 42Mhz
	USART2->BRR = 0x0683; // Baud rate of 9600, PLCK1 at 12Mhz
	// 6.Enable the Transmitter/Receiver by Setting the TE and RE bits in USART_CR1 register.
	USART2->CR1 |= (1<<3); // TE =1 -> Enable Transmitter
	USART2->CR1 |= (1<<2); // RE =1 -> Enable Receiver
}

void USART2_SendChar(uint8_t c)
{
	/******** STEPS FOLLOWER **********
	 * 1. Write the data to send in the USART_DR register (this clears the TXE bit). Repeat this
	 * for each data to be transmitted in case of single buffer.
	 * 2. After writing the last data into the USART_DR register, wait until TC=1.This indicates
	 * that the transmission of the last frame is complete. This is required for instance when the
	 * USART is disable or enter the Halt mode to void corrupting the last transmission.
	 */
	USART2->DR = c; // Load the data into DR register
	while(!(USART2->SR & (1<<6)));// Wait for TC to set .This indicates that the data has been transmitted
}

void USART2_SendString(char *string) // khai bao 1 mang
{
	char *ch = string; // khai bao con tro ch tro vao mang string
	while(*ch) // kiem tra gia tri cua mang
	{
	 USART2_SendChar(*ch); // gui gia tri
	 ch++; // tang dia chi con tro
	}
}

uint8_t USART2_GetChar(void)
{
	/*********** STEP FOLLOWS ***************
	 * 1.Wait for RXNE bit to SET . It indicates that the data has been received and can be read.
	 * 2.Read the data from USART_DR register. This also clears the RXNE bit
	 */
	uint8_t temp;
	while (!(USART2->SR & (1<<5))); // wait for RXNE bit to SET
    temp = USART2->DR; // Read the data. This also clears the RXNE also
	return temp;
}

// ***********************************************************************************

//***************************USART_DMA************************************************

void USART2_DMA_Config(void)
{
	/*********   Steps followed ***************

		1. Enable the UART CLOCK and GPIO Clock
		2. Configure the UART PINs for Alternate Functions
		3. Enable the UART by writing the UE bit in USART_CR1 register to 1.
		4. Program the M bit in USART_CR1 to define the word length.
		5. Enable DMA for Transmit
		6. Select the desired baud rate using the UART_BRR register.
		7. Enable the Transmitter/Receiver by Setting the TE and RE bits in USART_CR1 register.

		*******************************************/

		// 1.Enable the UART CLOCK and GPIO Clock
		RCC->APB1ENR |= (1<<17);
		RCC->AHB1ENR |= (1<<0);
		// 2.Configure the UART PINs for Alternate Functions
		GPIOA->MODER |= (2<<4); // Bits(5:4)=1:0--> Alternate Functions for Pin PA2
		GPIOA->MODER |= (2<<6); // Bits(7:6)=1:0--> Alternate Functions for Pin PA3
		GPIOA->OSPEEDR |= (3<<4)|(3<<6); // Bits (5:4) = 1:1 and Bits (7:6)=1:1 -> High Speed for PIN PA2 and PA3
		GPIOA->AFR[0] |= (7<<8); // Bytes (11:10:9:8) = 0:1:1:1 -> AF7 Alternate Functions for USART2 at PIN PA2
		GPIOA->AFR[0] |=(7<<12); // Bytes (15:14:13:12) = 0:1:1:1 -> AF7 Alternate Functions for USART2 at PIN PA3
		// 3.Enable the UART by writing the UE bit in USART_CR1 register to 1.
		USART2->CR1 = 0x00; // Clear all
		USART2->CR1 |= (1<<13); // USART enable "bit UE =1 "
		// 4.Program the M bit in USART_CR1 to define the word length.
		USART2 ->CR1 &= ~(1<<12); // M=0 , 1 bit start ,8 data, n bit stop
		USART2->CR2 =0x00; // 1 bit stop
		// 5. Enable DMA for Transmit
		USART2->CR3 |= (1<<6); // Enable DMA for Receive
		USART2->CR3 |= (1<<7); // Enable DMA for Transmit
		// 6.Select the desired baud rate using the UART_BRR register.
		/*
		 * Tx/Rx baud = PCLK/(16*USARTDIV)
		 * ex: USARTDIV = 19.53125
		 * -> MENSTISSA = 19;
		 * -> FRACTION = 16*0.53125 = 8.5
		 * -> USART2->BRR = (8<<0)|(19<<4) //  Baud rate of 115200, PLCK1 at 36Mh
		 */
	  //USART2->BRR = (13<<0)|(22<<4); // Baud rate of 115200, PLCK1 at 42Mhz
		USART2->BRR = 0x0683; // Baud rate of 9600, PLCK1 at 12Mhz
		// 7.Enable the Transmitter/Receiver by Setting the TE and RE bits in USART_CR1 register.
		USART2->CR1 |= (1<<3); // TE =1 -> Enable Transmitter
		USART2->CR1 |= (1<<2); // RE =1 -> Enable Receiver
}

void DMA_Init(void)
{
	  /* Configure DMAy Streamx: */
	  /* Set CHSEL bits according to DMA_CHSEL value */
	  /* Set DIR bits according to DMA_DIR value */
	  /* Set PINC bit according to DMA_PeripheralInc value */
	  /* Set MINC bit according to DMA_MemoryInc value */
	  /* Set PSIZE bits according to DMA_PeripheralDataSize value */
	  /* Set MSIZE bits according to DMA_MemoryDataSize value */
	  /* Set CIRC bit according to DMA_Mode value */
	  /* Set PL bits according to DMA_Priority value */
	  /* Set MBURST bits according to DMA_MemoryBurst value */
	  /* Set PBURST bits according to DMA_PeripheralBurst value */

	// 1.Enable DMA1 Clock
	RCC->AHB1ENR |= (1<<21);
	// 2.Set CHSEL bits according to DMA_CHSEL value
	DMA1_Stream6->CR |= (4<<25); // Channel 4 USART2_TX
	// 2.Enable DMA Interrupts
//	DMA1_Stream6->CR |= (1<<2)|(1<<3)|(1<<4); // TCIE,HTIE,TEIE Enable
	// 3.Set the Data Direction->DIR
//	DMA1_Stream6->CR &= ~(3<<6); // Peripheral to memory
//	DMA1_Stream6->CR |= (1<<6); //  Memory to Peripheral
	DMA1_Stream6->CR |= (2<<6); //  Memory to Memory
    // 4.Set PINC bit according to DMA_PeripheralInc value
	DMA1_Stream6->CR &= ~(1<<9); // Disable
	// 5.Enable the memory increment MINC
	DMA1_Stream6->CR |= (1<<10);
	// 6.Set the Peripheral data size PSIZE
	DMA1_Stream6->CR &= ~(3<<11); // 8 bit data --> Gia tri dich sang phai 13 lan
	// 7.Set the Memory data size MSIZE
	DMA1_Stream6->CR &= ~(3<<13); // 8 bit data --> (gia tri << vi tri dich phai)
	// 8.Enable the circular mode CIRC
	DMA1_Stream6->CR |= (1<<8);
	// 9.Set the Priority Lever -> PL
	DMA1_Stream6->CR &= ~(3<<16); // PL =0
	// 10.Set MBURST bits according to DMA_MemoryBurst value
	DMA1_Stream6->CR &= ~(3<<23);
	// 11.Set PBURST bits according to DMA_PeripheralBurst value
	DMA1_Stream6->CR &= ~(3<<21);
}

void DMA1_Config_TX(uint32_t *srcAdd, uint32_t *desAdd, uint16_t dataSize)
{
	//1.Set the data size in NDTR register
	DMA1_Stream6->NDTR = (uint16_t)dataSize;
	//2.Set the peripheral address in PAR register
	DMA1_Stream6->PAR =  (uint32_t)srcAdd;
	//3.Set the Memory address in MAR register
	DMA1_Stream6->M0AR =(uint32_t)desAdd;
	//4.Enable the DMA1
	DMA1_Stream6->CR |= (1<<0);
}

/************************DMA***************************
void DMA1_Stream6_IRQHandler(void)
{
  if((DMA1->HISR)&(1<<20))
  {
	  memcpy(&MaiBuf[index],&RxBuf[0],RXSIXE/2);
	  DMA1->HIFCR |= (1<<20);
	  index = index+(RXSIXE/2);
	  if(index>49) index =0;
  }

  if((DMA1->HISR)&(1<<21))
  {
	  memcpy(&MaiBuf[index],&RxBuf[RXSIXE/2],RXSIXE/2);
	  DMA1->HIFCR |= (1<<21);
	  index = index+(RXSIXE/2);
	  if(index>49) index =0;
  }
}
*************************************************************************************/


//**************************************ADC*******************************************

void ADC_Init(void)
{
	/*********************STEP TO FOLLOW*********************************
	 * 1.Enable ADC and GPIO clock
	 * 2.Set the prescalar in the the common Control register (CCR)
	 * 3.Set the Scan Mode and Resolution in the Control Register 1 (CR1)
	 * 4.Set the Continuous Conversion, EOC, and Data Alignment in the Control register 2(CR2)
	 * 5.Set the Sampling Time for the channels in ADC_SMPRx
	 * 6.Set the Regular channel sequence length in ADC_SQR1
	 * 7.Set the Respective GPIO PINs in the Analog Mode
	 */

  // 1.Enable ADC and GPIO clock
  RCC->APB2ENR |= (1<<8); //Enable clock ADC1
  RCC->AHB1ENR |= (1<<0); //Enable clock GPIOA
  // 2.Set the prescalar in the the common Control register (CCR)
  ADC->CCR |= (1<<16); // PCLK2 divide by 4
  // 3.Set the Scan Mode and Resolution in the Control Register 1 (CR1)
  ADC1->CR1 |= (1<<8); // SCAN mode enable
  ADC1->CR1 &= ~(1<<24); // 12 bit RES
  // 4.Set the Continuous Conversion, EOC, and Data Alignment in the Control register 2(CR2)
  ADC1->CR2 |= (1<<1); //enable continuous conversion mode
  ADC1->CR2 &= ~(1<<10); // EOC after each conversion
  ADC1->CR2 &= ~(1<<11); // Data Alignment Right
  // 5.Set the Sampling Time for the channels in ADC_SMPRx
  ADC1->SMPR2 &= ~((1<<3)|(1<<12)); // Sampling time of 3 cycles for channel 1 and channel 4
  // 6.Set the Regular channel sequence length in ADC_SQR1
  ADC1->SQR1 |= (1<<20); // SQR1_L = 1 for 2 conversions
  // 7.Set the Respective GPIO PINs in the Analog Mode
  GPIOA->MODER |= (3<<2); // Analog mode for PA1 (channel 1)
  GPIOA->MODER |= (3<<8); // Analog mode for PA4 (channel 4)
}

void ADC_Enable(void)
{
	/*******************STEP TO FOLLOW*********************************
	 * 1.Enable the ADC by setting ADON bit in CR2
	 * 2.Wait for ADC to sabilize  (approx 10us)
	 */
	ADC1->CR2 |= (1<<0); // ADON = 1 enable ADC1
	uint32_t delay = 10000;
	while(delay--);
}
void ADC_Start(int channel)
{
	/*******************STEP TO FOLLOW ********************************
	 * 1.Set the channel Sequence in the SQR Register
	 * 2.Clear the status Register
	 * 3.Start the Conversion by Setting the SWSTART bit in CR2
	 */
	/* Since we will be polling for each channel, here we will keep one channel in the sequence
	 * at a time ADC1->SQR3 |= (channel<<0)
	 * Will just keep the respective channel in the sequence for the conversion
	 */
	ADC1->SQR3 = 0;
	ADC1->SQR3 |= (channel <<0); // Conversion in regular sequence

	ADC1->SR =0; // Clear the status register
	ADC1->CR2 |= (1<<30); // Start the conversion
}
void ADC_WaitForConv(void)
{
	/*
	 * EOC Flag will be set, once the conversion is finished
	 */
	while(!(ADC1->SR & (1<<1))); // Wait for EOC flag to set
}
uint16_t ADC_GetVal(void)
{
	return ADC1->DR ; // Read the data registers
}
void ADC_Disable(void)
{
	/************************STEP TO FOLLOW*****************************************
	 * 1.Disable the ADC by Clearing ADON bit in CR2
	 */
	ADC1->CR2 &= ~(1<<0); // Disable ADC
}
/**************************************ADC_DMA***************************************
 *
 */
void ADC_DMA_Init(void)
{
	/*********************STEP TO FOLLOW*********************************
	 * 1.Enable ADC and GPIO clock
	 * 2.Set the prescalar in the the common Control register (CCR)
	 * 3.Set the Scan Mode and Resolution in the Control Register 1 (CR1)
	 * 4.Set the Continuous Conversion, EOC, and Data Alignment in the Control register 2(CR2)
	 * 5.Set the Sampling Time for the channels in ADC_SMPRx
	 * 6.Set the Regular channel sequence length in ADC_SQR1
	 * 7.Set the Respective GPIO PINs in the Analog Mode
	 */

  // 1.Enable ADC and GPIO clock
  RCC->APB2ENR |= (1<<8); //Enable clock ADC1
  RCC->AHB1ENR |= (1<<0); //Enable clock GPIOA
  // 2.Set the prescalar in the the common Control register (CCR)
  ADC->CCR |= (1<<16); // PCLK2 divide by 4
  // 3.Set the Scan Mode and Resolution in the Control Register 1 (CR1)
  ADC1->CR1 |= (1<<8); // SCAN mode enable
  ADC1->CR1 &= ~(1<<24); // 12 bit RES
  // 4.Set the Continuous Conversion, EOC, and Data Alignment in the Control register 2(CR2)
  ADC1->CR2 |= (1<<1); //enable continuous conversion mode
  ADC1->CR2 |= (1<<10); // EOC after each conversion
  ADC1->CR2 &= ~(1<<11); // Data Alignment Right
  // 5.Set the Sampling Time for the channels in ADC_SMPRx
 // ADC1->SMPR2 &= ~((1<<3)|(1<<12)); // Sampling time of 3 cycles for channel 1 and channel 4
  ADC1->SMPR2 &= ~((7<<3)|(7<<12)); // Sampling time of 3 cycles for channel 1 and channel 4
  // 6.Set the Regular channel sequence length in ADC_SQR1
 // ADC1->SQR1 |= (1<<20); // SQR1_L = 1 for 2 conversions
  ADC1->SQR1 |= (2<<20); // SQR1_L = 2 for 3 conversions
  // 7.Set the Respective GPIO PINs in the Analog Mode
  GPIOA->MODER |= (3<<2); // Analog mode for PA1 (channel 1)
  GPIOA->MODER |= (3<<8); // Analog mode for PA4 (channel 4)
  // 8.Enable DMA for ADC
  ADC1->CR2 |= (1<<8);
  // 9.Enable Continuous Request
  ADC1->CR2 |= (1<<9);
  // 10.Channel Sequence
  ADC1->SQR3 |= (1<<0);// SEQ1 for Channel 1
  ADC1->SQR3 |= (4<<5); // SEQ2 for Channel 4
  ADC1->SQR3 |= (18<<10); // SEQ2 for Channel 18
  /*
   *            Temp sensor
   */
  //* Sampling Freq for Temp sensor
  ADC1->SMPR1 |= (7<<24); // Sampling time of 21us
  // Set the TSVREFE Bit to wake the sensor
  ADC->CCR |= (1<<23);
}

void ADC_DMA_Enable(void)
{
	/*******************STEP TO FOLLOW*********************************
	 * 1.Enable the ADC by setting ADON bit in CR2
	 * 2.Wait for ADC to sabilize  (approx 10us)
	 */
	ADC1->CR2 |= (1<<0); // ADON = 1 enable ADC1
	uint32_t delay = 10000;
	while(delay--);
}

void ADC_DMA_Start(void)
{
	/*******************STEP TO FOLLOW ********************************
	 * 1.Clear the status Register
	 * 2.Start the Conversion by Setting the SWSTART bit in CR2
	 */
	ADC1->SR =0; // Clear the status register
	ADC1->CR2 |= (1<<30); // Start the conversion
}
void DMA2_Init(void)
{
	// Enable the DMA2 Clock
	RCC->AHB1ENR |= (1<<22); // DMA2EN=1
	// Select the Data Direction
	DMA2_Stream0->CR &=~(3<<6); // Peripheral to memory
	// Select Circular mode
	DMA2_Stream0->CR |=(1<<8); // CIRC =1
	// Enable Memory Address Increment
	DMA2_Stream0->CR |=(1<<10); // MINC =1
	//Set the size for data
	DMA2_Stream0->CR |=(1<<11)|(1<<13); // PSIZE =01; MSIZE = 01, 16 bit data
	//Select channel for stream
	DMA2_Stream0->CR &=~(7<<25); // Channel 0 selected

}
void DMA_Config (uint32_t srcAdd, uint32_t desAdd, uint8_t size)
{
	/********************STEP TO FOLLOW***************************
	 * 1.Set the Data Size in the NDTR Register
	 * 2.Set the Peripheral Address and Memory Address
	 * 3.Enable the DMA Stream
	 * Some peripherals don't need a start condition, like UART, So as soon as you enable the DMA
	 * ,the transfer will begin while Peripherals like ADC needs the Start condition, so start the ADC
	 * later in the program, to enable the tranfer
	 */
	DMA2_Stream0->NDTR = size ; // Set the size of the transfer
	DMA2_Stream0->PAR = srcAdd; // Source address is peripheral address
	DMA2_Stream0->M0AR = desAdd; // Destination Address is memory address
	DMA2_Stream0->CR |= (1<<0); // Enable the DMA Stream0->EN=1
}
// ************************************EXTIO******************************************

void EXTI0_Config()
{
	RCC->APB2RSTR |= (1<<14); // Tao xung clock cho chan APB2RSTR
//    SYSCFG -> EXTICR1 = 0x00;
	EXTI->RTSR = 0x00;
	EXTI -> FTSR |= (1<<0);
	EXTI -> IMR |= (1<<0);
	EXTI->PR |=(1<<0); // xoa co ngat
	NVIC -> ISER[0] = 0x40;
}

void EXTI0_IRQHandler(void)
{
	if((EXTI->PR ==1U)&&(EXTI -> IMR == 1U))
	{
	 EXTI ->PR = 0x01;
	 if( GPIOD->ODR&(0x1000))
	 {
	   GPIOD->ODR &= ~(1<<12);
	 }
	 else
	 {

   	GPIOD->ODR |= (1<<12);
	 }
	}
}

//**************************************************************************************
int main()
{
 // SysClockConfig();
  IO_Config();
 // EXTI0_Config(); // Interrup ANI0
 // RTC_Init();  // RTC
 // USART2_Config();
 // USART2_DMA_Config();
 // DMA_Init();
 // DMA1_Config_TX((uint32_t*)str1, (uint32_t*)addUSART_DR, strlen((char*)str1));
 // DMA1_Config_TX((uint32_t*)str1, (uint32_t*)str2, strlen((char*)str1));
 // **************ADC***********************
 // ADC_Init();
 // ADC_Enable();
 //***************ADC_DMA*******************
  ADC_DMA_Init();
  ADC_DMA_Enable();
  DMA2_Init();
  DMA_Config((uint32_t)&ADC1->DR,(uint32_t)RxData,3);
  ADC_DMA_Start();
  while(1)
  {
	/*******************GPIO*****************
      if(GPIOA->IDR&1)
	  {
         GPIOD->ODR |= (1<<12);
      }
         else
	  {
		 GPIOD->ODR &= ~(1<<12);
	  }
	delay_us(100000);
    GPIOD->ODR &= ~(1<<12);
	delay_us(100000);
	*/
	/*******RTC***************
	  RTC_get_time();
	  RTC_get_date();
	  */
	/********USART2*************
	  USART2_SendChar('a');
	  USART2_SendString("hello\n");
	  uint8_t data = USART2_GetChar();
	  USART2_SendChar(data);
	  delay_us(1000000);
	  */
	//*********ADC*******************************
	 /*
	  ADC_Start(1);
	  ADC_WaitForConv();
	  ADC_VAL[0] = ADC_GetVal();

	  ADC_Start(4);
	  ADC_WaitForConv();
	  ADC_VAL[1]= ADC_GetVal();

	  GPIOD->ODR |= (1<<12);
	  delay_us(ADC_VAL[0]*1000);
      GPIOD->ODR &= ~(1<<12);
      delay_us(ADC_VAL[0]*1000);
      */
	  //*****************ADC_DMA*****************
	  Temperature = (((float)(3.3*RxData[2]/(float)4095)-0.76)/0.0025)+25;
	  delay_us(1000000);
  }
  return 0;
}





